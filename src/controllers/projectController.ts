import { Request, Response } from "express-serve-static-core";

export default class projectController 
{
    static showForm(req: Request, res: Response): void
    {
        const db = req.app.locals.db;
        //let client = db.prepare ('SELECT client_id FROM Projet').all()
        let client2 = db.prepare ('SELECT client_id FROM Projet').all()
        console.log('new console :', client2)
        // console.log (req.body)
        //console.log(client[i].client_id)
        res.render('pages/projet-add', {
            title: 'Welcome',
            client : client2,
        });
    }
    static read (req: Request, res: Response): void
    {   
        let id = req.params.id
        const db = req.app.locals.db;
        let projet = db.prepare ('SELECT * FROM Projet WHERE id=?').get(id)
        res.render('pages/projet', {
            projet : projet
        });
    }
        
    static showAll (req: Request, res: Response): void 
    { 

        const db = req.app.locals.db;
        let reqAll = db.prepare('SELECT * from Projet').all()
        
        res.render('pages/index', {
            title: 'Gestion de projets',
            content:reqAll
        });
    }
    static add (req: Request, res: Response): void 
    { 
        console.log ('mon req: ', req.body)
        const db = req.app.locals.db;
        db.prepare('INSERT INTO Projet ("title", "content", "client_id") VALUES (?,?,?)').run(req.body.title, req.body.content, req.body.client_id[0])
        
        projectController.showAll (req, res);
    }
    static delete (req: Request, res: Response): void
    {
        const db = req.app.locals.db;
        db.prepare('DELETE FROM Projet where id=?').run(req.params.id)

        projectController.showAll (req, res);
    }



    static edit (req: Request, res: Response): void
    {
        const db = req.app.locals.db;
        db.prepare('UPDATE Projet SET title=?, content=? WHERE id=?').run(req.body.title, req.body.content, req.params.id)
    
        projectController.showAll (req, res);
    }

    static showFormEdit(req: Request, res: Response): void
    {
        const db = req.app.locals.db;
        let projet = db.prepare('SELECT * FROM Projet WHERE id=?').get(req.params.id)
        res.render('pages/projet-edit', {
            title: 'Welcome',
            projet: projet
        });
    }

}
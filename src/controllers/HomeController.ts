import { Request, Response } from "express-serve-static-core";

export default class HomeController
{
    static index(req: Request, res: Response): void
    {
        const db = req.app.locals.db;
        let projet = db.prepare ('SELECT * from Projet').all()
        res.render('pages/index', {
            title: 'Welcome',
            content : projet,
        });
    }

    static about(req: Request, res: Response): void
    {
        res.render('pages/about', {
            title: 'About',
        });
    }
}
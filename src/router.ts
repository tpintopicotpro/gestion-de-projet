import { Application } from "express";
import HomeController from "./controllers/HomeController";
import projectController from "./controllers/projectController";

export default function route(app: Application)
{
    /** Static pages **/
    app.get('/', (req, res) =>
    {
        projectController.showAll(req, res);
    });

    app.get('/about', (req, res) =>
    {
        HomeController.about(req, res);
    });

    app.get('/projet-add', (req, res) => 
    {
        projectController.showForm(req, res);
    });

    app.post('/projet-add', (req, res) =>
    {
        projectController.add(req, res);
    });

    app.get('/projet/:id', (req, res) =>
    {
        projectController.read (req, res);
    });

    app.get('/projet-delete/:id', (req, res) =>{
        projectController.delete(req, res);
    })

    app.get('/projet-edit/:id', (req, res) => 
    {
        projectController.showFormEdit(req, res);
    });

    app.post('/projet-edit/:id', (req, res) =>
    {
        projectController.edit(req, res);
    });
}

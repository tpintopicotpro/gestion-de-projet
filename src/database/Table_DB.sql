-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        Thomas
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2021-12-20 11:35
-- Created:       2021-12-20 09:31
PRAGMA foreign_keys = OFF;

-- Schema: mydb
-- ATTACH "mydb.sdb" AS "mydb";
BEGIN;
CREATE TABLE "client"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "nom" VARCHAR(45),
  "adresse" VARCHAR(45)
);
CREATE TABLE "dev"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "prenom" VARCHAR(45),
  "nom" VARCHAR(45),
  "niveau" VARCHAR(45)
);
CREATE TABLE "Projet"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "title" TEXT,
  "content" TEXT,
  "client_id" INTEGER NOT NULL,
  CONSTRAINT "fk_Projet_client1"
    FOREIGN KEY("client_id")
    REFERENCES "client"("id")
);
CREATE INDEX "Projet.fk_Projet_client1_idx" ON "Projet" ("client_id");
CREATE TABLE "dev_has_Projet"(
  "dev_id" INTEGER NOT NULL,
  "Projet_id" INTEGER NOT NULL,
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  CONSTRAINT "fk_dev_has_Projet_dev"
    FOREIGN KEY("dev_id")
    REFERENCES "dev"("id"),
  CONSTRAINT "fk_dev_has_Projet_Projet1"
    FOREIGN KEY("Projet_id")
    REFERENCES "Projet"("id")
);
CREATE INDEX "dev_has_Projet.fk_dev_has_Projet_Projet1_idx" ON "dev_has_Projet" ("Projet_id");
CREATE INDEX "dev_has_Projet.fk_dev_has_Projet_dev_idx" ON "dev_has_Projet" ("dev_id");
COMMIT;
